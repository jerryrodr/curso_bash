#!/bin/bash
#

numA=10
numB=4
clear
echo -e "\nOperadores aritméticos"
echo "Los números son A=$numA y B=$numB"
echo " Sumar $numA + $numB = " $((numA + numB))
echo " Restar $numA - $numB = " $((numA - numB))
echo " Multiplicar $numA * $numB = " $((numA * numB))
echo " Dividir $numA / $numB = " $((numA / numB))
echo " Residuo $numA % $numB = " $((numA % numB))

echo -e "\nOperadores relacionaes"
echo "Los números son A=$numA y B=$numB"
echo " $numA >  $numB = " $((numA > numB))
echo " $numA <  $numB = " $((numA < numB))
echo " $numA >= $numB = " $((numA >= numB))
echo " $numA <= $numB = " $((numA <= numB))
echo " $numA == $numB = " $((numA == numB))
echo " $numA != $numB = " $((numA != numB))

echo -e "\nOperadores de asignación"
echo "Los números son A=$numA y B=$numB"
echo " Sumar $numA += $numB = " $((numA += numB))
echo " Restar $numB -= $numA = " $((numB -= numA))
echo " Multiplicar $numA *= $numB = " $((numA *= numB))
echo " Dividir $numA /= $numB = " $((numA /= numB))
echo " Residuo $numA %= $numB = " $((numA %= numB))
echo -e "\n"

echo "Los números son A=$numA y B=$numB"
((numA++))
((numA++))
((++numA))
((++numA))
echo "Los números son A=$numA y B=$numB"

